# README #

Hyperbolic geometry appears to be intrinsic in many large real networks, giving rise to the generative model of hyperbolic random graphs. Several generative sampling algorithms for this model exists, including the Geometric Inhomogenuous Random Graphs (GIRG), an extension by Bringmann et al. In the repository of Anton Krohmer, an implementation of the GIRG generation algorithm can be found. This fork exists to benchmark this implementation for static and dynamic hyperbolic random graphs.

### Setup ###

* To compile, you need to install the libraries `gflags` (>=2.1.2), `glog` (>=0.3.4), and `gsl` (>=1.16). 
* Once that is done, compile with `make`. If it fails, double check that the includes of above libraries can be found in `/usr/local/include`, otherwise change the path in the makefile. The makefile also looks for the libraries in `/usr/local/lib`.

### Misc. ###

* If you want to use the static generator, please cite "Efficient Embedding of Scale-Free Graphs in the Hyperbolic Plane" by Bläsius et al., published at ESA 2016.
* For the dynamic generator, please cite "Generating massive complex networks with hyperbolic geometry faster in practice" by Looz et al., published at HPEC'16